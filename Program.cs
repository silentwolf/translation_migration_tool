﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;

namespace StringMigrator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            List<string> Sfilesnames = new List<string>();
            List<string> Dfilesnames = new List<string>();
            string source;
            string destination;

            //string result;

            Console.WriteLine("Input the location of the directory containing the translated files to use as source:");
            source = Console.ReadLine();
            Console.WriteLine("Input the location of the directory containing the original KR files to use as target:");
            destination = Console.ReadLine();

            DirectoryInfo s = new DirectoryInfo(source);
            DirectoryInfo d = new DirectoryInfo(destination);
            foreach (var file in s.GetFiles("*.LUA"))
            {
                if(!file.Name.Contains("QUEST"))
                Sfilesnames.Add(file.DirectoryName+"\\"+file.Name);
            }
            foreach (var file in d.GetFiles("*.LUA"))
            {
                if (!file.Name.Contains("QUEST"))
                    Dfilesnames.Add(file.DirectoryName + "\\" + file.Name);
            }
            
            

            string[,] stringtable = new string[900000,3];
            string[,] resulttable = new string[900000, 3];
            int[] placement = new int[900000];

            
            
            
            int found = 0;

            //Filling the array with strings & IDs
            foreach (var file in Sfilesnames)
            {
                int stringid = 0;
                int increment = 4;
                using (StringReader reader = new StringReader(File.ReadAllText(file, Encoding.UTF8)))
                {



                    string line = string.Empty;
                    do
                    {
                        line = reader.ReadLine();
                        stringid = stringid + 1;
                        if (stringid == increment && line != null)
                        {
                            stringtable[found, 0] = line;
                        }
                        if (stringid == increment+1 && line != null)
                        {
                            stringtable[found, 1] = line;
                            increment = increment + 5;
                            found = found + 1;
                        }



                    } while (line != null);
                }
                Console.WriteLine(file + " found.");
            }


            Console.WriteLine(found+" strings found.");

            //Checking the array's actual length

            int limit = 0;
            for (int i = 0; i < 900000; i++)
            {
                if (stringtable[i, 0] != null)
                    limit = i;
            }

            foreach (var file in Dfilesnames)
            {
                //Fetch matching ID's line number on destination file
                using (StringReader reader = new StringReader(File.ReadAllText(file, Encoding.UTF8)))
                {

                    for (int i = 0; i < limit; i++)
                    {
                        stringtable[i, 2] = null;
                    }

                    int linenumber = 1;
                    string line = string.Empty;
                    do
                    {
                        line = reader.ReadLine();
                        resulttable[linenumber, 0] = line;
                        for (int arrayindice = 0; arrayindice < limit; ++arrayindice)
                        {

                            if (stringtable[arrayindice, 0] == line)
                            {
                                stringtable[arrayindice, 2] = (linenumber + 1).ToString();
                                arrayindice = arrayindice + 1;
                                break;
                            }

                        }
                        linenumber = linenumber + 1;

                    } while (line != null);
                }

                int rowLengths = stringtable.GetLength(0);
                for (int i = 0; i < rowLengths; i++)
                {
                    if (stringtable[i, 2] != null)
                    {
                        resulttable[int.Parse(stringtable[i, 2]), 0] = stringtable[i, 1];
                    }
                }

                var last = Dfilesnames.Last();
                for (int i = 0; i < resulttable.GetLength(0); i++)
                {
                    if (resulttable[i, 0] != null)
                    {
                        using (System.IO.StreamWriter stream =
                        new System.IO.StreamWriter(file+"2", true))
                        {
                            if (file.Equals(last) == false)
                                stream.WriteLine(resulttable[i, 0]);
                            else
                            {
                                if (resulttable[i, 0] != "}")
                                    stream.WriteLine(resulttable[i, 0]);
                                else
                                {
                                    stream.WriteLine(resulttable[i, 0]);
                                    break;
                                }
                            }
                                
                        }
                    }
                }
                Console.WriteLine(file+" migrated successfully.");

            }

            Console.WriteLine("Translation migration complete.");

            Console.ReadKey();
        }
    }
}
